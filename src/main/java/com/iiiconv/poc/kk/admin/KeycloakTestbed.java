package com.iiiconv.poc.kk.admin;

import com.google.devtools.common.options.Option;
import com.google.devtools.common.options.OptionsParser;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RealmRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class KeycloakTestbed {
    //private static final String SERVER_URL = "http://ec2-52-87-187-103.compute-1.amazonaws.com:8180/auth";
    //public static final String SERVER_URL = "http://localhost:8180/auth";
    //public static final String SERVER_URL = "http://ec2-52-91-164-47.compute-1.amazonaws.com:8180/auth";
    private static final String REALM = "master";
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    private static final String CLIENT_ID = "admin-cli";

    public static void main(String[] args) {

        OptionsParser parser = OptionsParser.newOptionsParser(KeycloakTestbedOptions.class);
        parser.parseAndExitUponError(args);
        KeycloakTestbedOptions options = parser.getOptions(KeycloakTestbedOptions.class);

        if (options.setupMode)
            setUp(options);

        if (options.requestRealmsMode)
            requestRealms(options);

        if (options.requestTokensMode)
            requestTokens(options);

        if (options.refreshTokensMode)
            requestTokens(options);

    }
    public static void requestTokens (KeycloakTestbedOptions options) {
        Random random = new Random();
        List<GetTokenTask> tasks = new ArrayList<>();
        IntStream.range(options.realmsRangeMin, options.realmsRangeMax).forEach(
                i -> {
                    IntStream.range(0, options.usersPerRealm).forEach(
                            j -> tasks.add(new GetTokenTask(options.keycloakUrl, options.realmPrefix + i, "admin-cli",
                                options.userIdPrefix + random.ints(options.usersRangeMin, options.usersRangeMax+1).findFirst().getAsInt(),
                                "password")));
                }
        );
        tasks.stream().forEach(t -> t.start());
    }

    public static void refreshTokens (KeycloakTestbedOptions options) {
        Random random = new Random();
        List<RefreshTokenTask> tasks = new ArrayList<>();
        IntStream.range(options.realmsRangeMin, options.realmsRangeMax).forEach(
                i -> {
                    IntStream.range(0, options.usersPerRealm).forEach(
                            j -> tasks.add(new RefreshTokenTask(options.keycloakUrl,options.realmPrefix + i, "admin-cli",
                                    options.userIdPrefix + random.ints(options.usersRangeMin, options.usersRangeMax+1).findFirst().getAsInt(),
                                    "password")));
                }
        );
        tasks.stream().forEach(t -> t.start());
    }

    public static void requestRealms(KeycloakTestbedOptions options){
        GetRealmsTask task = new GetRealmsTask(options.keycloakUrl, "master", "admin-cli", "admin", "admin");
        task.start();
    }

    public static void setUp(KeycloakTestbedOptions options) {
        Keycloak keycloak = KeycloakBuilder
                .builder()
                .serverUrl(options.keycloakUrl)
                .realm(REALM)
                .username(USERNAME)
                .password(PASSWORD)
                .clientId(CLIENT_ID)
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(1).build())
                .build();

        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue("password");

        try {
            for (Integer i = options.realmsRangeMin; i < options.realmsRangeMax; i++) {
                RealmRepresentation realmRepresentation = new RealmRepresentation();
                realmRepresentation.setRealm(options.realmPrefix + i.toString());
                realmRepresentation.setEnabled(true);
                realmRepresentation.setSslRequired("NONE");
                keycloak.realms().create(realmRepresentation);

                for (Integer j = options.usersRangeMin; j < options.usersRangeMax; j++) {
                    UserRepresentation userRepresentation = new UserRepresentation();
                    userRepresentation.setUsername(options.userIdPrefix + j.toString());
                    userRepresentation.setFirstName(options.userIdPrefix);
                    userRepresentation.setLastName(j.toString());
                    userRepresentation.setEnabled(true);
                    userRepresentation.setCredentials(Arrays.asList(credentialRepresentation));
                    keycloak.realm(options.realmPrefix + i.toString()).users().create(userRepresentation);
                }

                ClientRepresentation clientRepresentation = new ClientRepresentation();
                clientRepresentation.setClientId("login-client");
                keycloak.realm(options.realmPrefix + i.toString()).clients().create(clientRepresentation);
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class KeycloakTask extends Thread {
    protected Keycloak keycloak;
    protected String id;

    public KeycloakTask(String url, String realm, String clientTd, String userId, String password) {
        this.keycloak = KeycloakBuilder
                .builder()
                .serverUrl(url)
                .realm(realm)
                .username(userId)
                .password(password)
                .clientId(clientTd)
                .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
                .build();

        this.id = realm + '.' + userId;
    }
}

class GetTokenTask extends KeycloakTask {

    public GetTokenTask (String url, String realm, String clientTd, String userId, String password) {
        super(url, realm, clientTd, userId, password);
    }

    @Override
    public void run() {
        long start;
        long finish;
        String token;
        while (true) {
            start = System.currentTimeMillis();
            token = this.getToken();
            finish = System.currentTimeMillis();
            System.out.println(this.id + " " + (finish - start) + ": " + token);
            this.invalidateToken(token);
        }
    }

    private String getToken() {
        return this.keycloak.tokenManager().getAccessToken().getToken();
    }

    private void invalidateToken(String t) { this.keycloak.tokenManager().invalidate(t); }
}

class RefreshTokenTask extends KeycloakTask {

    public RefreshTokenTask(String url, String realm, String clientTd, String userId, String password) {
        super(url, realm, clientTd, userId, password);
    }

    @Override
    public void run() {
        long start;
        long finish;
        String token;
        while (true) {
            start = System.currentTimeMillis();
            token = this.refreshToken();
            finish = System.currentTimeMillis();
            System.out.println(this.id + " " + (finish - start) + ": " + token);
        }
    }

    private String refreshToken() {
        return this.keycloak.tokenManager().refreshToken().getToken();
    }
}



class GetRealmsTask extends KeycloakTask {

    public GetRealmsTask(String url, String realm, String clientTd, String userId, String password) {
        super(url, realm, clientTd, userId, password);
    }

    @Override
    public void run() {
        long start;
        long finish;
        while (true) {
            start = System.currentTimeMillis();
            this.getRealms().stream()
                    .map(r -> r.getRealm())
                    .forEach(System.out::println);
            finish = System.currentTimeMillis();
            System.out.println("Latency: " + (finish - start));
        }
    }
    private List<RealmRepresentation> getRealms() { return this.keycloak.realms().findAll(); }
}
