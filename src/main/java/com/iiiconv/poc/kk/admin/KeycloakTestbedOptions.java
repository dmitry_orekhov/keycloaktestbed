package com.iiiconv.poc.kk.admin;

import com.google.devtools.common.options.Option;
import com.google.devtools.common.options.OptionsBase;

public class KeycloakTestbedOptions extends OptionsBase {
    public KeycloakTestbedOptions() {
        super();
    }

    @Option(
            name = "setup-mode",
            abbrev = 'S',
            help = "Setup mode",
            defaultValue = "false"
    )
    public boolean setupMode;

    @Option(
            name = "keycloak-url",
            defaultValue = "http://localhost:8180/auth"
    )
    public String keycloakUrl;

    @Option(
            name = "realms-range-min",
            help = "Set up number of realms",
            defaultValue = "0"
    )
    public int realmsRangeMin;

    @Option(
            name = "realms-range-max",
            help = "Set up number of realms",
            defaultValue = "1"
    )
    public int realmsRangeMax;

    @Option(
            name = "realms-prefix",
            help = "Realms name prefix",
            defaultValue = ""
    )
    public String realmPrefix;

    @Option(
            name = "users-range-min",
            help = "Users per realm",
            defaultValue = "0"
    )
    public int usersRangeMin;

    @Option(
            name = "users-range-max",
            help = "Users per realm",
            defaultValue = "1"
    )
    public int usersRangeMax;

    @Option(
            name = "users-prefix",
            help = "Users Id prefix",
            defaultValue = "user"
    )
    public String userIdPrefix;


    @Option(
            name = "users-per-realm",
            help = "Users Id prefix",
            defaultValue = "1"
    )
    public int usersPerRealm;

    @Option(
            name = "request-tokens",
            help = "Launch test",
            defaultValue = "false"
    )
    public boolean requestTokensMode;

    @Option(
            name = "refresh-tokens",
            help = "Launch test",
            defaultValue = "false"
    )
    public boolean refreshTokensMode;

    @Option(
            name = "request-realms",
            help = "Launch test",
            defaultValue = "false"
    )
    public boolean requestRealmsMode;
}
